import datetime
from datetime import datetime, timedelta
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import plotly
import boto3
import dash_auth
from dash.dependencies import Input, Output, State


import pandas as pd
import numpy as np
import re
import threading

import plotly.plotly as py
import plotly.graph_objs as go
import colorlover as cl
from IPython.display import HTML
from flask import Flask

import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
# import server as application



VALID_USERNAME_PASSWORD_PAIRS = [
    ['admin', 't2monitoringsystem2019'],
    ['diordanov', 't2monitoringsystem2019'],
    ['mtrepanier', 't2monitoringsystem2019'],
    ['ghaumont', 't2monitoringsystem2019'],
    ['phopf', 't2monitoringsystem2019'],
    ['cbarajas', 't2monitoringsystem2019'],
    ['hkhalidy', 't2monitoringsystem2019']
]

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
server = Flask(__name__)
app = dash.Dash(__name__, server = server, external_stylesheets=external_stylesheets)
app.css.config.serve_locally = True
app.scripts.config.serve_locally = True
application = app.server
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)

sites = {}
ldt = ''

def getDataFromS3():
    client = boto3.client('s3') 
    resource = boto3.resource('s3') 

    obj2 = client.get_object(Bucket='tsquaredhadoop', Key='stats/dashboard/sites_list.txt')
    sl = pd.read_csv(obj2['Body'], sep=',', header = 0)
    sites_ext = {}
    bucket = resource.Bucket('tsquaredhadoop') 
    for site in list(sl['sitename']):
        frames = {}
        names = {}
        for obj in bucket.objects.filter(Prefix='stats/'+site):
            m = re.search('stats\/'+site+'\.(.+?).txt', obj.key)
            if m:
                name = m.group(1)
                names[name] = obj.key
        for k,v in names.items():
            objc = client.get_object(Bucket='tsquaredhadoop', Key=v)
            df = pd.read_csv(objc['Body'], sep=',', header = 0, error_bad_lines=False)
            #remove duplicates, keep last added
            df.drop_duplicates(subset = ['date','domain','metric'], keep = 'last', inplace = True)
            frames[k] = df
        sites_ext[site] = frames
    return sites_ext


def checkMetrics():
    sender_email = "t2monitoringsys@gmail.com"  # Enter your address
    receiver_email = [
                        'amartin@tsquaredinsights.com'
                        , 'diordanov@tsquaredinsights.com'
                        , 'pwinkler@tsquaredinsights.com'
                        ]  # Enter receiver address
    password = "Monitoringsystem123"

    message = MIMEMultipart("alternative")
    message["Subject"] = "system alarm"
    message["From"] = sender_email
    message["To"] = ", ".join(receiver_email)

    # Create the plain-text and HTML version of your message
    text = """
    This is an automatically generated alert. 
    The table below contains the metrics and respective websites on which values are out of normal range:"""

    html = """<html>
      <body>
      <style>
             table, td, th, tr {
                border: 1px solid darkgray;
                width: 300px;
                font-weight: normal;
                font-size: 12px;
                font-family: monospace;
                }
            td {
                text-align: center; 
                vertical-align: middle;
             }
             p, br {
                 font-size: 12px;
                 font-family: monospace;
             }
             th {
                 background-color: #A2B5CD;
                 font-weight: bold;
             }
             table {
                width: 70%; 
                margin: auto;
             }
          </style>
        <p>Dear email recipient,
        <br>
        <br>This is an automatically generated alert.<br><br>
           The table below contains the metrics and respective websites on which recently received values are out of normal range:
        </p>
    """

    alerts = {'Website':[], 'Metric':[], 'Value':[], 'Normal range':[]}
    metrics = ['Add to cart', 'unique_ids', 'errors']
    for site in sites.keys():
            for domain in sites[site].keys():
                dfr = sites[site][domain]
                tbl = pd.pivot_table(dfr, values='count', index=['date'], columns=['metric'], aggfunc=np.sum)
                l = len(tbl.index)
                date = datetime.today() - timedelta(days=4)
                date = date.strftime("%Y-%m-%d")
                # only recently updated websites 
                if tbl.index[l-1] >= date:
                    date = datetime.today() - timedelta(days=369)
                    date = date.strftime("%Y-%m-%d")
                    # consider 1 year if exists
                    if date in tbl.index: 
                        for metric in metrics:
                            if metric in tbl.columns and pd.notna(tbl[metric][l-1]):
                                if metric == 'errors':
                                    # if num of errors < 1% of product page views
                                    prod_page = tbl['Product page'][l-1]
                                    if tbl['errors'][l-1] > 0.01 * prod_page:
                                        alerts['Metric'].append('errors')
                                        alerts['Website'].append('{}.{}'.format(site, domain))
                                        alerts['Value'].append(tbl['errors'][l-1])
                                        alerts['Normal range'].append('< {}'.format(round(0.01 * prod_page)))
                                else:
                                    mean = tbl.loc[date:][metric][:l-2].mean()
                                    std = tbl.loc[date:][metric][:l-2].std()
                                    new_value = tbl[metric][l-1]
                                    if tbl.loc[date:][metric][:l-2].mean() <= tbl.loc[date:][metric][:l-2].median() and (mean - 2.5 * std) > 0:
                                        if not (mean - 2.5 * std) <= new_value <= (mean + 3 * std):
                                            alerts['Metric'].append(metric)
                                            alerts['Website'].append('{}.{}'.format(site, domain))
                                            alerts['Value'].append(tbl[metric][l-1])
                                            alerts['Normal range'].append('{} - {}'.format(round(mean - 2.5 * std), round(mean + 3 * std)))
                                    else:
                                        min_val = tbl.loc[date:][metric][:l-2].min() 
                                        if not min_val <= new_value <= (mean + 3 * std):
                                            alerts['Metric'].append(metric)
                                            alerts['Website'].append('{}.{}'.format(site, domain))
                                            alerts['Value'].append(tbl[metric][l-1])
                                            alerts['Normal range'].append('{} - {}'.format(round(min_val), round(mean + 3 * std)))
    if len(alerts['Metric']) > 0:
        alerts_df = pd.DataFrame(alerts)
        alerts_html = alerts_df.to_html(col_space = 30, index = False)
        # Turn these into plain/html MIMEText objects
        html = html + alerts_html + """<p><br>Sincerely yours,<br>T2 Monitoring System</br></p></body></html>"""
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")
        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
    )


def checkMetricsDaily():
    threading.Timer(86400.0, checkMetricsDaily).start()
    global sites
    sites = getDataFromS3()
    print(list(sites.keys())[0])
    checkMetrics()
    #threading.Timer(300.0, checkMetricsDaily).start()
   

checkMetricsDaily()


def serve_layout():
    hd = html.Div([
        html.Div([
            html.Div([
                html.Label('Website'),
                dcc.Dropdown(id='selected website',
                        style={'height': '32px', 'width': '200px'},
                         multi=False,
                         options=[{'label': n, 'value': n} for n in sites.keys()], 
                         value=list(sites.keys())[0],
                         placeholder='Website')], style={'marginBottom': 5, 'marginTop': 15, 'display': 'inline-block', 'margin-right': 15}),
            html.Div([
                html.Label('Domain'),
                dcc.Dropdown(id='selected domain',
                        style={'height': '32px', 'width': '200px'},
                         multi=False,
                         options=[], 
                         value=[],
                         placeholder='Domain')], style={'marginBottom': 5, 'marginTop': 15, 'display': 'inline-block', 'margin-right': 15}),
            html.Div([
                html.Div([
                    html.Label('Period')], style={'marginBottom': 0, 'marginTop': 0, 'margin-right': 0}),
                html.Div([
                    dcc.RadioItems(id='selected interval',
                        options=[
                            {'label': '1W', 'value': '1W'},
                            {'label': '1M', 'value': '1M'},
                            {'label': '3M', 'value': '3M'},
                            {'label': '6M', 'value': '6M'},
                            {'label': '1Y', 'value': '1Y'},
                            {'label': 'all', 'value': 'all'}
                        ],
                        value='all',
                        labelStyle={'display': 'inline-block', 'padding': '5px'})
                    ], style={'marginBottom': 5, 'marginTop': 0, 'margin-right': 10, 'border': '1px solid #ccc', 'border-radius': '4px', 'padding': '0px'})]
                , style = {'marginBottom': 5, 'marginTop': 15, 'float': 'right', 'margin-right': 15})]
            , style = {'marginBottom': 0, 'marginTop': 0, 'margin-right': 0}),
        html.Div([
            html.Label('Metric'),
            dcc.Dropdown(id='selected metric',
                    style={'height': '37px', 'width': '100%'},
                    multi=True,
                    options=[], 
                    value=[],
                    placeholder='Select metric')], style={'marginBottom': 5, 'marginTop': 5, 'margin-right': 20}),
        dcc.Graph(id='live-update-graph'),
        # html.Div([
        #     dcc.Graph(id='live-update-graph')], style={'marginBottom': 25, 'marginTop': 30}),
        html.Div([
            html.Label(ldt, id='lbl'),
            dash_table.DataTable( id='metrics_table',
                columns=[{"name": i, "id": i} for i in ['Metric', 'Value']],
                style_cell={'textAlign': 'center'},
                style_as_list_view=True,
                style_cell_conditional=[
                {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(248, 248, 248)'
                }],
                style_header={
                'backgroundColor': 'white',
                'fontWeight': 'bold'
                 },
                #filtering=True,
                sorting=True,
                sorting_type="multi"
                )
        ], style = {'width': '25%', 'margin':'auto'})
    ], style = {'font-family': 'monospace'})

    return hd

app.layout = serve_layout


def get_data(selected_metric, selected_domain, selected_website, selected_interval):
        #print(selected_domain)
        #print(frames[selected_domain])
        data = {}
        if selected_website and selected_domain and selected_metric:
            for i in selected_metric:
                data[i] = []
            data['date'] = []
            date = []
            frames = sites[selected_website]
            if selected_domain:
                df = frames[selected_domain]
                table = pd.pivot_table(df, values='count', index=['date'], columns=['metric'], aggfunc=np.mean)
                d1 = datetime.strptime(table.index[0], "%Y-%m-%d")  # start date
                d2 = datetime.strptime(table.index[len(table.index)-1], "%Y-%m-%d")  # end date
                delta = d2 - d1       # timedelta
                fdd = {'date':[], 'Number':[]}
                for i in range(delta.days + 1):
                    fdd['date'].append((d1 + timedelta(days=i)).strftime("%Y-%m-%d"))
                    fdd['Number'].append(0)
                fdf = pd.DataFrame.from_dict(fdd)
                fdf.set_index('date',inplace = True)
                table = fdf.join(table)
                del table['Number']
                num_days = 0
                if selected_interval == '1W':
                    num_days = 7
                if selected_interval == '1M':
                    num_days = 30
                elif selected_interval == '6M':
                    num_days = 180
                elif selected_interval == '3M':
                    num_days = 90
                elif selected_interval == '1Y':
                    num_days = 365
                elif selected_interval == 'all':
                    num_days = len(table.index)
                if (len(table.index) - num_days) >= 0:
                    num_periods = len(table.index) - num_days
                else: 
                    num_periods = 0
                for dt in table.index[num_periods:]:
                    date.append(dt)
                    for l in data.keys():
                        if l != 'date':
                            val = table.loc[dt][l]
                            data[l].append(val)
                        else:
                            data[l].append(dt)
        print('The data for the selected website, domain and time period is retrieved...')
        return data

# Multiple components can update everytime interval gets fired.
@app.callback(Output('live-update-graph', 'figure'), 
              [Input('selected metric', 'value'), Input('selected domain', 'value'), Input('selected website', 'value'), Input('selected interval', 'value')]
               )
def update_graph(selected_metric, selected_domain, selected_website, selected_interval):
    if selected_metric and selected_domain and selected_website:
        traceslist = [] 
        layout_dict = {}
        if selected_metric:
            data = get_data(selected_metric, selected_domain, selected_website, selected_interval)
            #HTML(cl.to_html( cl.scales['11'] ))
            #colors = cl.scales['11']['qual']['Paired']
            colors2 = ['#332288',   
                       '#D55E00',
                       '#E69F00',  
                       '#CC6677', 
                       '#117733',  
                       '#44AA99',  
                       '#AA4499',  
                       '#7f7f7f',  
                       '#882255',  
                       '#0072B2',
                       '#0072B2',
                       '#4B0092'
                                   ]


            traces = {}
            for inx, l in enumerate(data.keys()):
                if l != 'date':
                    if inx != 0:
                        traces[l] = go.Scatter(
                        x=data['date'],
                        y=data[l],
                        name=l,
                        yaxis='y'+ str(inx+1),
                        line = dict(color = colors2[inx])
                        )
                    else:
                        traces[l] = go.Scatter(
                        x=data['date'],
                        y=data[l],
                        name=l                           
            )       
            for key, value in traces.items():
                temp = value
                traceslist.append(temp)    
            
            i = 0
            for inx, axis_name in enumerate(data.keys()):
                if axis_name != 'date':
                    key = "yaxis" if inx == 0 else ("yaxis%d" % (inx+1))
                    d = {}
                    d['title'] = axis_name
                    if inx != 0:
                        d['overlaying'] = 'y'
                    d['anchor'] = 'free'
                    if inx < len(data.keys())/2-1:
                        pos = inx*0.065
                        d['position'] = pos
                        d['side'] = 'left'
                    else:
                        pos = 1 - i*0.065-0.025
                        d['position'] = pos
                        d['side'] = 'right'
                        i+=1
                    c = {}
                    c['color'] = colors2[inx]
                    d['titlefont'] = c
                    d['tickfont'] = c
                    d['showgrid'] = False
                    d['showline'] = True
                    d['zeroline'] = False
                    layout_dict[key] = d

                    
            #layout_dict['title'] = "Title"
            #layout_dict['width'] = 1500
            dm = {}
            xpos1 = (len(data.keys())/2-1)*0.065
            xpos2 = 1 - (len(data.keys())/2-1)*0.065 -0.03
            dm['domain'] = [xpos1, xpos2]
            dm['showline'] = True
            dm['zeroline'] = False
            dm['showgrid'] = False
            layout_dict['xaxis'] = dm
            layout_dict['font']=dict(family='Courier New, monospace')
            #layout_dict['showline'] = False
        layout = go.Layout(
            layout_dict
        )
            #traceslist
            #layout_dict
        fig = go.Figure(data=traceslist, layout=layout)   
        fig['layout']['margin'] = {
             'l': 50, 'r': 10, 'b': 30, 't': 50
         }
        return fig
        return go.Figure()
    else:
        return {'data': []}

@app.callback(Output('metrics_table', 'data'),
                  [Input('selected metric', 'value'), Input('selected domain', 'value'), Input('selected website', 'value')])
def update_metrics_table(selected_metric, selected_domain, selected_website):
    if selected_metric and selected_domain and selected_website:
        table = pd.pivot_table(sites[selected_website][selected_domain], values='count', index=['date'], columns=['metric'], aggfunc=np.mean)
        dt = table.index.max()
        lastday = {}
        data = get_data(selected_metric, selected_domain, selected_website, '1M')
        for l in data.keys():
            if l != 'date':
                lastday[l] = table.loc[dt][l]
        return pd.DataFrame(lastday.items(), columns=['Metric', 'Value']).to_dict('records')
    else:
        return ""

@app.callback(Output('selected domain', 'options'),
                [Input('selected website', 'value')])
def updateDomainOptions(selected_website):
    options = []
    value = ""
    if selected_website:
        options=[{'label':n, 'value':n} for n in sites[selected_website].keys()]
    return options

@app.callback(Output('selected metric', 'options'),
    [Input('selected website', 'value'),
        Input('selected domain', 'value')])
def updateMetricOptions(selected_website, selected_domain):
    options = []
    if selected_website and selected_domain:
        options=[{'label':n, 'value':n} for n in set(sites[selected_website][selected_domain]['metric'])]
    return options


@app.callback(Output('selected domain', 'value'),
                [Input('selected website', 'value')])
def updateDomainValue(selected_website):
    if selected_website:
        return list(sites[selected_website].keys())[0]

@app.callback(Output('selected metric', 'value'),
            [Input('selected website', 'value'),
            Input('selected domain', 'value')])
def updateMetricValue(selected_website, selected_domain):
    values = []
    defaultValues = []
    if selected_website and selected_domain:
        values = list(set(sites[selected_website][selected_domain]['metric']))
        print(values)
        # default pre-selected metrics
        for m in ['total_rows', 'unique_ids', 'errors']:
            indx = values.index(m)
            if indx:
                print(values[indx])
                defaultValues.append(values[indx])
            else:
                print(values.index(m))

    return defaultValues

@app.callback(Output('lbl', 'children'),
            [Input('selected website', 'value'),
            Input('selected domain', 'value')])
def updateMetricValue(selected_website, selected_domain):
    table = pd.pivot_table(sites[selected_website][selected_domain], values='count', index=['date'], columns=['metric'], aggfunc=np.mean)
    dt = table.index.max()
    ldt = 'Last update received on {}:'.format(dt)
    return ldt




if __name__ == '__main__':
    application.run(debug=True, port=8080, use_reloader=False)

