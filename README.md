## T2 Stats DashBoard
The T2 Stats Dashboard is an interactive dashboard which visualizes activitiy metrics on the websites T2 is receiving data for. The metrics are collected daily and live in an Amazon S3 bucket.  

### Tools
The dashboard is an web application deployed with the AWS Elastic Beanstalk service and build with Dash. Dash is a Python framework for building analytical web applications. 

### Authentication
The app is avaiable [here] [l1] and uses HTTP Basic Auth authentication. Set of usernames and passwords are hardcoded in the app code. The limitation of this approach is that users cannot log out of the application, create their own account, and change their password. 

### Dash and Interactivity
Dash is a Python framework for building web applications, written on top of Flask, Plotly.js, and React.js
The dashboard is made interactive through Dash Callbacks: Python functions that are automatically called whenever an input component's property changes. Callbacks can be chained, allowing one update in the UI to trigger several updates across the app.

### Data
Every time the app web page is loaded the data is retreived from the AWS S3 bucket and stored locally on the EC2 instance on which the AWS Elasic Beanstalk service is running. 

### Core components
The Dashboard comprises the following elements: 
* The `Website` dropdown allows to select a group of websites (the websites are groupped by name, e.g. "amazon", "walmart")
* The `Domain` dropdown allows to select an available domain for the chosen website group (e.g., "amazon.com" or "amazon.de")
* The `Metric` dropdown lists available metrics for the chosen website group. Multiple metrics can be selected at once. Every time website or domain is changed, all available metrics are pre-selected. The metrics can be unselected by closing them directly in the dropdown field. 
* The `Period` radio buttons group allows to switch between different time periods: 
    * 1 weeek
    * 1 month
    * 3 months
    * 6 months
    * 1 year
    * all available data points
Choosing the last option will ensure that data points for all available periods will be displayed.
* The `Graph` with multiple lines for each metric currently being selected versus time (x-axis). The graph has a separate y-axis for each of the selected metrics. The axes and lines are color-coded respectively. Every time a metric is (de)selected, the graph is re-rendered with corresponding number of y-axes.  The `legend` is located on the right-hand side and allows to make lines (in)visible by clicking corresponding legend element.
* The `Table` underneath the Graph shows figures for the metrics currently being selected within the Metric dropdown at the last available date. The table fields can be sorted by value. 

### Additional graph interactivity

On hover over a data point two modes are available: 
* `Compare data` renders values of all data points for the selected date
* `Show closest data` renders value of the data point closest to the cursor
The mode is to be selected in the right upper corner of the graph (depicted as one or two direction pointers). 

Selecting a rectangular area with a cursor on the graph will zoom in the selected area. Double-click anywhere in the plotting area to zoom out or use the Reset axes button (A house icon). 

### Adding a new item to the list of available websites
Given that the statistics file for the new website resides at `s3://tsquaredhadoop/stats/` , in order to add a website, add the name of the website (excluding domain) to the `sites_list.txt` living at `s3://tsquaredhadoop/stats/dashboard/` .

### Running a new version of the application
At the [elastibeanstalk] [l2] console (All Applications > T2DashBoard > Dashboard), press the `Upload and Deploy` button, and upload a zip archive consisting of three files: 
*  application.py
*  .ebextensions
*  requirements.txt

### License
Dash is an open source library, released under the permissive MIT license

### Estimated cost
The AWS Elastic Beanstalk is a free service. It is running on an EC2 instance of the t2.micro type which costs $0.013/hour or $9.50/month.
Due to a low expected load of the application, the Load Balancer service which requires additional chargers is not activated. 

### Contact
Please [email][mt] in case of any question. 

[l1]:http://t2dashboard-env.645zhzqvqa.eu-central-1.elasticbeanstalk.com/
[l2]:https://eu-central-1.console.aws.amazon.com/elasticbeanstalk/home?region=eu-central-1#/environment/dashboard?applicationName=T2DashBoard&environmentId=e-pncyufa26s
[mt]:mailto:amartin@tsquaredinsight